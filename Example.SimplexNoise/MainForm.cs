﻿using System;
using System.Drawing;
using System.Windows.Forms;
using SandWar.Math.Random;

namespace Example.SimplexNoise
{
	public partial class MainForm : Form
	{
		const float timeScale = 0.5f;
		
		float scale = 1f/50;
		int octaves = 6;
		float persistence = 0.7f;
		float postMultiply = 1f;
		
		readonly SandWar.Math.Noise.SimplexNoise noise;
		Bitmap bmp;
		float z;
		DateTime lastFrame;
		
		public MainForm() {
			InitializeComponent();
			
			noise = new SandWar.Math.Noise.SimplexNoise();
			
			resizeRenderBitmap();
			lastFrame = DateTime.Now;
		}
		
		void PictureBoxResize(object sender, EventArgs e) {
			resizeRenderBitmap();
		}
		
		void RandomizeButtonClick(object sender, EventArgs e) {
			var random = new RandomGenerator();
			scale = random.GetFloat(1/100f, 1/20f);
			octaves = random.GetInteger(2, 6);
			persistence = random.GetFloat(0.2f, 1f);
			postMultiply = (float)Math.Pow(random.GetFloat(0.75f, 1.5f), 5f);
			
			z -= 1f;
			
			render();
			pictureBox.Invalidate();
		}
		
		void FrameButtonClick(object sender, EventArgs e) {
			z -= 1f;
			
			render();
			pictureBox.Invalidate();
		}
		
		void AnimationButtonClick(object sender, EventArgs e) {
			timer.Enabled = !timer.Enabled;
		}
		
		void TimerTick(object sender, EventArgs e) {
			var thisFrame = DateTime.Now;
			z += (float)(thisFrame - lastFrame).TotalSeconds * timeScale;
			lastFrame = thisFrame;
			
			render();
			pictureBox.Invalidate();
		}
		
		void resizeRenderBitmap() {
			bmp = new Bitmap(pictureBox.Width, pictureBox.Height);
			pictureBox.Image = bmp;
		}
		
		void render() {
			for(int y = 0; y < bmp.Height; y++) {
				for(int x = 0; x < bmp.Width; x++) {
					var v = (int)((compute(x*scale, y*scale, z)/2f + 0.5f)*255f);
					v = Math.Max(0, Math.Min(255, v));
					
					bmp.SetPixel(x, y, Color.FromArgb(255, v, v, v));
				}
			}
		}
		
		float compute(float x, float y, float z) {
			float v = 0f;
			float mul = 1f;
			float freq = 1f;
			
			for(int i=1; i<octaves; i++) {
				v += noise.Compute(x*freq, y*freq, z*freq)*mul;
				mul *= persistence;
				freq *= 2f;
			}
			
			return v*postMultiply;
		}
	}
}
