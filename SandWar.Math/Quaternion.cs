﻿using System;

namespace SandWar.Math
{
	public struct Quaternion
	{
		/// <summary>X value.</summary>
		public float X;
		/// <summary>Y value.</summary>
		public float Y;
		/// <summary>Z value.</summary>
		public float Z;
		/// <summary>W value.</summary>
		public float W;
		
		public static Quaternion Euler(Vector3 eulerAngles) {
			// http://stackoverflow.com/a/11505219/1135019
			
			double yaw = eulerAngles.X * Math.Pi / 180;
			double pitch = eulerAngles.Y * Math.Pi / 180;
			double roll = eulerAngles.Z * Math.Pi / 180;
			
			double rollOver2 = roll * 0.5d;
			double sinRollOver2 = Math.Sin(rollOver2);
			double cosRollOver2 = Math.Cos(rollOver2);
			double pitchOver2 = pitch * 0.5f;
			double sinPitchOver2 = Math.Sin(pitchOver2);
			double cosPitchOver2 = Math.Cos(pitchOver2);
			double yawOver2 = yaw * 0.5f;
			double sinYawOver2 = Math.Sin(yawOver2);
			double cosYawOver2 = Math.Cos(yawOver2);
			
			return new Quaternion {
				X = (float)(cosYawOver2 * cosPitchOver2 * cosRollOver2 + sinYawOver2 * sinPitchOver2 * sinRollOver2),
				Y = (float)(cosYawOver2 * cosPitchOver2 * sinRollOver2 - sinYawOver2 * sinPitchOver2 * cosRollOver2),
				Z = (float)(cosYawOver2 * sinPitchOver2 * cosRollOver2 + sinYawOver2 * cosPitchOver2 * sinRollOver2),
				W = (float)(sinYawOver2 * cosPitchOver2 * cosRollOver2 - cosYawOver2 * sinPitchOver2 * sinRollOver2)
			};
		}
		
		public static Vector3 operator *(Quaternion q, Vector3 v) {
			float x2 = q.X * 2f;
			float y2 = q.Y * 2f;
			float z2 = q.Z * 2f;
			
			float xx = x2 * q.X;
			float yy = y2 * q.Y;
			float zz = z2 * q.Z;
			
			float xy = y2 * q.X;
			float xz = z2 * q.X;
			float xw = x2 * q.W;
			
			float yz = z2 * q.Y;
			float yw = y2 * q.W;
			
			float zw = z2 * q.W;
			
			return new Vector3(
				(1f - (yy + zz)) * v.X + (xy - zw) * v.Y + (xz + yw) * v.Z,
				(xy + zw) * v.X + (1f - (xx + zz)) * v.Y + (yz - xw) * v.Z,
				(xz - yw) * v.X + (yz + xw) * v.Y + (1f - (xx + yy)) * v.Z
			);
		}
	}
}
