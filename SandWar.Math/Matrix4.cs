using System;

namespace SandWar.Math
{
	/// <summary>A 4x4 matrix of floats.</summary>
	public struct Matrix4
	{
		float
			v00, v01, v02, v03,
			v10, v11, v12, v13,
			v20, v21, v22, v23,
			v30, v31, v32, v33;
		
		#region Constructors and identity
		
		Matrix4(Matrix4 m) {
			v00 = m.v00; v01 = m.v01; v02 = m.v02; v03 = m.v03;
			v10 = m.v10; v11 = m.v11; v12 = m.v12; v13 = m.v13;
			v20 = m.v20; v21 = m.v21; v22 = m.v22; v23 = m.v33;
			v30 = m.v30; v31 = m.v31; v32 = m.v32; v33 = m.v33;
		}
		
		/// <summary>The identity matrix.</summary>
		public static readonly Matrix4 Identity = new Matrix4 {
			v00 = 1, v11 = 1, v22 = 1, v33 = 1
		};
		
		/// <summary>Perspective matrix.</summary>
		public static Matrix4 Perspective(float fovy, float aspect, float near, float far) {
			var f  = 1f / Math.Tan(fovy / 2);
			var nf = 1f / (near - far);
			
			return new Matrix4 {
				v00 = f / aspect,
				v11 = f,
				v22 = (far + near) * nf,
				v23 = -1f,
				v32 = (2f * far * near) * nf,
				v33 = 0f
			};
		}
		
		#endregion
		
		#region Index getters
		
		public float this[int i] {
			get {
				switch(i) {
					case 0: return v00;
					case 1: return v01;
					case 2: return v02;
					case 3: return v03;
					case 4: return v10;
					case 5: return v11;
					case 6: return v12;
					case 7: return v13;
					case 8: return v20;
					case 9: return v21;
					case 10: return v22;
					case 11: return v23;
					case 12: return v30;
					case 13: return v31;
					case 14: return v32;
					case 15: return v33;
					default: throw new IndexOutOfRangeException();
				}
			}
			set {
				switch(i) {
					case 0: v00 = value; break;
					case 1: v01 = value; break;
					case 2: v02 = value; break;
					case 3: v03 = value; break;
					case 4: v10 = value; break;
					case 5: v11 = value; break;
					case 6: v12 = value; break;
					case 7: v13 = value; break;
					case 8: v20 = value; break;
					case 9: v21 = value; break;
					case 10: v22 = value; break;
					case 11: v23 = value; break;
					case 12: v30 = value; break;
					case 13: v31 = value; break;
					case 14: v32 = value; break;
					case 15: v33 = value; break;
					default: throw new IndexOutOfRangeException();
				}
			}
		}
		
		public float this[int x, int y] {
			get { return this[x + y * 4]; }
			set { this[x + y * 4] = value; }
		}
		
		#endregion
		
		#region Properties
		
		/// <summary>Compute the determinant of this matrix.</summary>
		public float Determinant {
			get {
				return
					(v00 * v11 - v01 * v10) * (v22 * v33 - v23 * v32) -
					(v00 * v12 - v02 * v10) * (v21 * v33 - v23 * v31) +
					(v00 * v13 - v03 * v10) * (v21 * v32 - v22 * v31) +
					(v01 * v12 - v02 * v11) * (v20 * v33 - v23 * v30) -
					(v01 * v13 - v03 * v11) * (v20 * v32 - v22 * v30) +
					(v02 * v13 - v03 * v12) * (v20 * v31 - v21 * v30);
			}
		}
		
		/// <summary>Invert this matrix.</summary>
		public Matrix4 Inverted {
			get {
				var b00 = v00 * v11 - v01 * v10;
				var b01 = v00 * v12 - v02 * v10;
				var b02 = v00 * v13 - v03 * v10;
				var b03 = v01 * v12 - v02 * v11;
				var b04 = v01 * v13 - v03 * v11;
				var b05 = v02 * v13 - v03 * v12;
				var b06 = v20 * v31 - v21 * v30;
				var b07 = v20 * v32 - v22 * v30;
				var b08 = v20 * v33 - v23 * v30;
				var b09 = v21 * v32 - v22 * v31;
				var b10 = v21 * v33 - v23 * v31;
				var b11 = v22 * v33 - v23 * v32;
				
				var invDet = 1 / Determinant;
				
				return new Matrix4 {
					v00 = (v11 * b11 - v12 * b10 + v13 * b09) * invDet,
					v01 = (v02 * b10 - v01 * b11 - v03 * b09) * invDet,
					v02 = (v31 * b05 - v32 * b04 + v33 * b03) * invDet,
					v03 = (v22 * b04 - v21 * b05 - v23 * b03) * invDet,
					v10 = (v12 * b08 - v10 * b11 - v13 * b07) * invDet,
					v11 = (v00 * b11 - v02 * b08 + v03 * b07) * invDet,
					v12 = (v32 * b02 - v30 * b05 - v33 * b01) * invDet,
					v13 = (v20 * b05 - v22 * b02 + v23 * b01) * invDet,
					v20 = (v10 * b10 - v11 * b08 + v13 * b06) * invDet,
					v21 = (v01 * b08 - v00 * b10 - v03 * b06) * invDet,
					v22 = (v30 * b04 - v31 * b02 + v33 * b00) * invDet,
					v23 = (v21 * b02 - v20 * b04 - v23 * b00) * invDet,
					v30 = (v11 * b07 - v10 * b09 - v12 * b06) * invDet,
					v31 = (v00 * b09 - v01 * b07 + v02 * b06) * invDet,
					v32 = (v31 * b01 - v30 * b03 - v32 * b00) * invDet,
					v33 = (v20 * b03 - v21 * b01 + v22 * b00) * invDet
				};
			}
		}
		
		/// <summary>Transpose values of this matrix.</summary>
		public Matrix4 Transposed {
			get {
				return new Matrix4 {
					v00 = v00, v01 = v10, v02 = v20, v03 = v30,
					v10 = v01, v11 = v11, v12 = v21, v13 = v31,
					v20 = v02, v21 = v12, v22 = v22, v23 = v32,
					v30 = v03, v31 = v13, v32 = v23, v33 = v33
				};
			}
		}
		
		#endregion
		
		#region Operators
		
		public static Matrix4 operator *(Matrix4 lhs, Matrix4 rhs) {
			return new Matrix4 {
				v00 = rhs.v00*lhs.v00 + rhs.v01*lhs.v10 + rhs.v02*lhs.v20 + rhs.v03*lhs.v30,
				v01 = rhs.v00*lhs.v01 + rhs.v01*lhs.v11 + rhs.v02*lhs.v21 + rhs.v03*lhs.v31,
				v02 = rhs.v00*lhs.v02 + rhs.v01*lhs.v12 + rhs.v02*lhs.v22 + rhs.v03*lhs.v32,
				v03 = rhs.v00*lhs.v03 + rhs.v01*lhs.v13 + rhs.v02*lhs.v23 + rhs.v03*lhs.v33,
				v10 = rhs.v10*lhs.v00 + rhs.v11*lhs.v10 + rhs.v12*lhs.v20 + rhs.v13*lhs.v30,
				v11 = rhs.v10*lhs.v01 + rhs.v11*lhs.v11 + rhs.v12*lhs.v21 + rhs.v13*lhs.v31,
				v12 = rhs.v10*lhs.v02 + rhs.v11*lhs.v12 + rhs.v12*lhs.v22 + rhs.v13*lhs.v32,
				v13 = rhs.v10*lhs.v03 + rhs.v11*lhs.v13 + rhs.v12*lhs.v23 + rhs.v13*lhs.v33,
				v20 = rhs.v20*lhs.v00 + rhs.v21*lhs.v10 + rhs.v22*lhs.v20 + rhs.v23*lhs.v30,
				v21 = rhs.v20*lhs.v01 + rhs.v21*lhs.v11 + rhs.v22*lhs.v21 + rhs.v23*lhs.v31,
				v22 = rhs.v20*lhs.v02 + rhs.v21*lhs.v12 + rhs.v22*lhs.v22 + rhs.v23*lhs.v32,
				v23 = rhs.v20*lhs.v03 + rhs.v21*lhs.v13 + rhs.v22*lhs.v23 + rhs.v23*lhs.v33,
				v30 = rhs.v30*lhs.v00 + rhs.v31*lhs.v10 + rhs.v32*lhs.v20 + rhs.v33*lhs.v30,
				v31 = rhs.v30*lhs.v01 + rhs.v31*lhs.v11 + rhs.v32*lhs.v21 + rhs.v33*lhs.v31,
				v32 = rhs.v30*lhs.v02 + rhs.v31*lhs.v12 + rhs.v32*lhs.v22 + rhs.v33*lhs.v32,
				v33 = rhs.v30*lhs.v03 + rhs.v31*lhs.v13 + rhs.v32*lhs.v23 + rhs.v33*lhs.v33
			};
			
			//if(destination === a || destination === b) {
			//	if(destination === a && destination === b)
			//		a = b = Matrix4.copy(buffer, destination);
			//	else if(destination === a)
			//		a = Matrix4.copy(buffer, destination);
			//	else
			//		b = Matrix4.copy(buffer, destination);
			//}
			//
			//for(var y=0; y<4; y++)
			//	for(var x=0; x<4; x++)
			//		destination[y*4+x] =
			//			b[y*4+ 0] * a[ 0+x] +
			//			b[y*4+ 1] * a[ 4+x] +
			//			b[y*4+ 2] * a[ 8+x] +
			//			b[y*4+ 3] * a[12+x];
		}
		
		#endregion
		
		#region Methods
		
		/// <summary>Translate this matrix by a vector.</summary>
		public Matrix4 Translate(Vector3 translation) {
			return new Matrix4 (this) {
				v30 = v00*translation.X + v10*translation.Y + v20*translation.Z + v30,
				v31 = v01*translation.X + v11*translation.Y + v21*translation.Z + v31,
				v32 = v02*translation.X + v12*translation.Y + v22*translation.Z + v32,
				v33 = v03*translation.X + v13*translation.Y + v23*translation.Z + v33,
			};
		}
		
		#endregion
	}
}

