﻿using System;
using System.Collections.Generic;

namespace SandWar.Math
{
	/// <summary>A vector containing 3 floats.</summary>
	public struct Vector3
	{
		#region Constants
		
		/// <summary>Vector at the origin.</summary>
		public static readonly Vector3 Origin = new Vector3( 0f,  0f,  0f);
		/// <summary>Vector at Z +1.</summary>
		public static readonly Vector3 Front  = new Vector3( 0f,  0f, +1f);
		/// <summary>Vector at Z -1.</summary>
		public static readonly Vector3 Back   = new Vector3( 0f,  0f, -1f);
		/// <summary>Vector at Y +1.</summary>
		public static readonly Vector3 Up     = new Vector3( 0f, +1f,  0f);
		/// <summary>Vector at Y -1.</summary>
		public static readonly Vector3 Down   = new Vector3( 0f, -1f,  0f);
		/// <summary>Vector at X +1.</summary>
		public static readonly Vector3 Left   = new Vector3(+1f,  0f,  0f);
		/// <summary>Vector at X -1.</summary>
		public static readonly Vector3 Right  = new Vector3(-1f,  0f,  0f);
		
		#endregion
		
		/// <summary>X value.</summary>
		public float X;
		/// <summary>Y value.</summary>
		public float Y;
		/// <summary>Z value.</summary>
		public float Z;
		
		/// <summary>Construct a vector.</summary>
		/// <param name="x">X value</param>
		/// <param name="y">Y value</param>
		/// <param name="z">Z value</param>
		public Vector3(float x = 0f, float y = 0f, float z = 0f) {
			this.X = x;
			this.Y = y;
			this.Z = z;
		}
		
		#region Properties
		
		/// <summary>
		/// Get the square of the magnitude of this vector.
		/// 
		/// This method is faster than <see cref="Magnitude"/>.
		/// </summary>
		public float SquareMagnitude {
			get { return X*X + Y*Y + Z*Z; }
		}
		
		/// <summary>Get the magnitude of this vector.</summary>
		/// <seealso cref="SquareMagnitude"/>
		public float Magnitude {
			get { return Math.Sqrt(SquareMagnitude); }
			set { this = Normalized * value; }
		}
		
		/// <summary>Get the normalized version of this vector.</summary>
		public Vector3 Normalized {
			get {
				var magnitude = Magnitude;
				return (magnitude < Math.Epsilon) ? (Origin) : (this / magnitude);
			}
		}

		/// <summary>Get the absolute value of this vector.</summary>
		public Vector3 Absolute {
			get { return new Vector3(Math.Abs(X), Math.Abs(Y), Math.Abs(Z)); }
		}
		
		#endregion
		
		#region Operators
		
		public static Vector3 operator +(Vector3 v1, Vector3 v2) {
			return new Vector3(
				v1.X + v2.X,
				v1.Y + v2.Y,
				v1.Z + v2.Z
			);
		}
		
		public static Vector3 operator -(Vector3 v1, Vector3 v2) {
			return new Vector3(
				v1.X - v2.X,
				v1.Y - v2.Y,
				v1.Z - v2.Z
			);
		}
		
		public static Vector3 operator -(Vector3 v) {
			return new Vector3(
				-v.X,
				-v.Y,
				-v.Z
			);
		}
		
		public static Vector3 operator *(Vector3 v, float f) {
			return new Vector3(
				v.X * f,
				v.Y * f,
				v.Z * f
			);
		}
		
		public static Vector3 operator *(float f, Vector3 v) {
			return new Vector3(
				f * v.X,
				f * v.Y,
				f * v.Z
			);
		}
		
		public static Vector3 operator /(Vector3 v, float f) {
			return new Vector3(
				v.X / f,
				v.Y / f,
				v.Z / f
			);
		}
		
		public static bool operator ==(Vector3 lhs, Vector3 rhs) {
			return (lhs - rhs).SquareMagnitude < Math.Epsilon;
		}
		
		public static bool operator !=(Vector3 lhs, Vector3 rhs) {
			return (lhs - rhs).SquareMagnitude >= Math.Epsilon;
		}
		
		#endregion
		
		#region Methods
		
		/// <summary>Compute the dot product between this vector and another.</summary>
		/// <param name="other">The other vector.</param>
		/// <returns>The dot product.</returns>
		public float Dot(Vector3 other) {
			return this.X*other.X + this.Y*other.Y + this.Z*other.Z;
		}
		
		#endregion
		
		#region Static Methods
		
		/// <summary>Find the smaller of components of two <see cref="Vector3"/>.</summary>
		public static Vector3 Min(Vector3 v1, Vector3 v2) {
			return new Vector3(Math.Min(v1.X, v2.X), Math.Min(v1.Y, v2.Y), Math.Min(v1.Z, v2.Z));
		}
		
		/// <summary>Find the bigger of components of two <see cref="Vector3"/>.</summary>
		public static Vector3 Max(Vector3 v1, Vector3 v2) {
			return new Vector3(Math.Max(v1.X, v2.X), Math.Max(v1.Y, v2.Y), Math.Max(v1.Z, v2.Z));
		}
		
		#endregion
		
		#region Overrides
		
		public override bool Equals(object obj) {
			if (obj is Vector3)
				return Equals((Vector3)obj);
			
			return false;
		}
		
		public bool Equals(Vector3 other) {
			return Math.Abs(X - other.X) < Math.Epsilon &&
			       Math.Abs(Y - other.Y) < Math.Epsilon &&
			       Math.Abs(Z - other.Z) < Math.Epsilon;
		}
		
		public override int GetHashCode() {
			// disable NonReadonlyReferencedInGetHashCode
			return unchecked(((17 * 29 + X.GetHashCode()) * 29 + Y.GetHashCode()) * 29 + Z.GetHashCode());
			// enable NonReadonlyReferencedInGetHashCode
		}
		
		public override string ToString() {
			return string.Format("[{0}, {1}, {2}]", X, Y, Z);
		}
		
		#endregion
	}
}
