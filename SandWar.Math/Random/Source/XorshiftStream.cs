﻿using System;
using System.IO;
using SandWar.Math.Random;

namespace SandWar.Math.Random.Source
{
	/// <summary>
	/// A fast Xorshift RNG <see cref="Stream"/>.
	/// 
	/// If <see cref="UnsafeRead"/> is not used, this class is thread safe.
	/// </summary>
	public sealed class XorshiftStream : Stream
	{
		object toLock = new object();
		ulong x;
		
		/// <summary>Construct a Xorshift stream with the specified seed.</summary>
		/// <param name="seed">The seed.</param>
		public XorshiftStream(int seed) {
			if(seed == 0)  // Xorshift doesn't work with 0 seed
				seed = -1;
			
			x = unchecked( (ulong)seed );
			x ^= x >> 12;
			x ^= x << 25;
			x ^= x >> 27;
		}
		
		/// <summary>Construct a new Xorshift stream with a random seed.</summary>
		public XorshiftStream() : this( SeedGenerator.RandomSeed ) { }
		
		/// <summary>Does nothing.</summary>
		public override void Flush() { }
		
		/// <summary>Not supported.</summary>
		/// <exception cref="NotSupportedException">Always thrown.</exception>
		public override long Seek(long offset, SeekOrigin origin) {
			throw new NotSupportedException();
		}
		
		/// <summary>Not supported.</summary>
		/// <exception cref="NotSupportedException">Always thrown.</exception>
		public override void SetLength(long value) {
			throw new NotSupportedException();
		}
		
		/// <summary>Generates a sequence of random bytes.</summary>
		/// <returns>The total number of bytes read into the buffer, always <paramref name="count"/>.</returns>
		/// <param name="buffer">An array of bytes. When this method returns, the buffer contains the specified byte array with the values between <paramref name="offset"/> and (<paramref name="offset"/> + <paramref name="count"/> - 1) replaced by the bytes read from the current source.</param>
		/// <param name="offset">The zero-based byte offset in <paramref name="buffer"/> at which to begin storing the data read from the current stream.</param>
		/// <param name="count">The number of bytes to be read from the current stream.</param>
		/// <exception cref="T:System.ArgumentException">The sum of <paramref name="offset"/> and <paramref name="count"/> is larger than the buffer length.</exception>
		/// <exception cref="T:System.ArgumentNullException"><paramref name="buffer"/> is null.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException"><paramref name="offset"/> or <paramref name="count"/> is negative.</exception>
		public override int Read(byte[] buffer, int offset, int count) {
			if(buffer == null)
				throw new ArgumentNullException("buffer", "Buffer cannot be null.");
			
			if(offset < 0)
				throw new ArgumentOutOfRangeException("offset", "Index is less than zero.");
			
			if(count < 0)
				throw new ArgumentOutOfRangeException("count", "Index is less than zero.");
			
			if(buffer.Length - offset < count)
				throw new ArgumentException("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection.");
			
			lock(toLock)
				UnsafeRead(buffer, offset, count);
			
			return count;
		}
		
		/// <summary>Not supported.</summary>
		/// <exception cref="NotSupportedException">Always thrown.</exception>
		public override void Write(byte[] buffer, int offset, int count) {
			throw new NotSupportedException();
		}
		
		/// <summary>Always true.</summary>
		public override bool CanRead {
			get { return true; }
		}
		
		/// <summary>Always false.</summary>
		public override bool CanSeek {
			get { return false; }
		}
		
		/// <summary>Always false.</summary>
		public override bool CanWrite {
			get { return false; }
		}
		
		/// <summary>Not supported.</summary>
		/// <exception cref="NotSupportedException">Always thrown.</exception>
		public override long Length {
			get { throw new NotSupportedException(); }
		}
		
		/// <summary>Not supported.</summary>
		/// <exception cref="NotSupportedException">Always thrown.</exception>
		public override long Position {
			get { throw new NotSupportedException(); }
			set { throw new NotSupportedException(); }
		}
		
		/// <summary>
		/// Generate a sequence of random bytes, skipping some controls to improve performance.
		/// 
		/// Using this method will cause loss of thread safety in the object.
		/// </summary>
		/// <param name="buffer">An array of bytes. When this method returns, the buffer contains the specified byte array with the values between <paramref name="offset"/> and (<paramref name="offset"/> + <paramref name="count"/> - 1) replaced by the bytes read from the current source.</param>
		/// <param name="offset">The zero-based byte offset in <paramref name="buffer"/> at which to begin storing the data read from the current stream.</param>
		/// <param name="count">The number of bytes to be read from the current stream.</param>
		public unsafe void UnsafeRead(byte[] buffer, int offset, int count) {
			var _x = x;
			
			var temp = stackalloc byte[ sizeof(ulong) ];
			
			fixed(byte* buf = buffer) {
				var pos = buf + offset;
				var end = pos + count;
				while(pos < end) {
					_x ^= _x >> 12; // a
					_x ^= _x << 25; // b
					_x ^= _x >> 27; // c
					
					*( (ulong*)temp ) = unchecked( _x * 2685821657736338717ul );
					
					var tempend = count > sizeof(ulong) ? pos + sizeof(ulong) : end;
					var temppos = temp;
					while(pos < tempend)
						*(pos++) = *(temppos++);
				}
			}
			
			x = _x;
		}
	}
}
