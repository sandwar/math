﻿using System;
using System.Threading;

namespace SandWar.Math.Random
{
	/// <summary>Allows to generate random seeds to be used in random generators.</summary>
	public static class SeedGenerator
	{
		static readonly RandomGenerator random = new RandomGenerator( unchecked(Environment.TickCount - Thread.CurrentThread.ManagedThreadId) );
		
		/// <summary>Generate a random <see cref="Int32"/> seed.</summary>
		public static Int32 RandomSeed {
			get { return random.GetInt32(); }
		}
	}
}
