﻿using System;
using System.IO;
using SandWar.Math.Random.Source;

namespace SandWar.Math.Random
{
	/// <summary>Provides utilities to operate with random operations on arrays.</summary>
	public static class ArrayRandom
	{
		static readonly Stream randomStream = new XorshiftStream();
		
		/// <summary>Shuffle elements in the array randomly.</summary>
		/// <param name="array">The array to shuffle.</param>
		/// <param name="randomStream">The random number generator to use as scrambler.</param>
		public static void Shuffle<T>(this T[] array, Stream randomStream) {
			var rand = new RandomGenerator(randomStream);
			
			for(int i = 0; i < array.Length; i++)
				swap(array, i, rand.GetInteger(array.Length));
		}
		
		/// <summary>Shuffle elements in the array randomly.</summary>
		/// <param name="array">The array to shuffle.</param>
		/// <param name="index">The index to start scrambling at.</param>
		/// <param name="count">How many elements to scramble, from index.</param>
		/// <param name="randomStream">The random number generator to use as scrambler.</param>
		public static void Shuffle<T>(this T[] array, int index, int count, Stream randomStream) {
			var rand = new RandomGenerator(randomStream);
			
			for(int i = index; i < count; i++)
				swap(array, i, rand.GetInteger(index, index+count));
		}
		
		/// <summary>Shuffle elements in the array randomly.</summary>
		/// <param name="array">The array to shuffle.</param>
		public static void Shuffle<T>(this T[] array) {
			Shuffle<T>(array, randomStream);
		}
		
		/// <summary>Shuffle elements in the array randomly.</summary>
		/// <param name="array">The array to shuffle.</param>
		/// <param name="index">The index to start scrambling at.</param>
		/// <param name="count">How many elements to scramble, from index.</param>
		public static void Shuffle<T>(this T[] array, int index, int count) {
			Shuffle<T>(array, index, count, randomStream);
		}
		
		static void swap<T>(T[] array, int index1, int index2) {
			T buffer = array[index1];
			array[index1] = array[index2];
			array[index2] = buffer;
		}
	}
}
