﻿using System;
using System.IO;
using SandWar.Math.Random.Source;

namespace SandWar.Math.Random
{
	/// <summary>
	/// Wraps a random data stream to generate random values for various .NET types.
	/// This class is thread safe if the underlying stream is thead safe, but generated number are not totally evenly distributed.
	/// </summary>
	public class RandomGenerator
	{
		readonly Stream randomStream;
		
		byte[] randBuffer;
		
		/// <summary>Instantiate a <see cref="RandomGenerator"/> with the specified random data source.</summary>
		/// <param name="randomStream">The stream to use as random data source.</param>
		public RandomGenerator(Stream randomStream) {
			this.randomStream = randomStream;
			randBuffer = new byte[ 32 ];  // 32 is a number at least as high as the size of the largest generated data type
		}
		
		/// <summary>Instantiate a <see cref="RandomGenerator"/> with the specified seed.</summary>
		/// <param name="seed">The seed to use to generate random data.</param>
		public RandomGenerator(int seed) : this(new XorshiftStream(seed)) { }
		
		/// <summary>Instantiate a <see cref="RandomGenerator"/> with a random seed.</summary>
		public RandomGenerator() : this(SeedGenerator.RandomSeed) { }
		
		/// <summary>Get an array of random bytes.</summary>
		/// <param name="length">The length of the array of random bytes.</param>
		/// <returns>Array of random bytes.</returns>
		/// <exception cref="ObjectDisposedException">This generator has been disposed.</exception>
		public byte[] GetBytes(int length) {
			var bytes = new byte[length];
			GetBytes(bytes, 0, length);
			return bytes;
		}
		
		/// <summary>Generates a sequence of random bytes.</summary>
		/// <param name="buffer">An array of bytes. When this method returns, the buffer contains the specified byte array with the values between <paramref name="offset"/> and (<paramref name="offset"/> + <paramref name="count"/> - 1) replaced by the bytes read from the current source.</param>
		/// <param name="offset">The zero-based byte offset in <paramref name="buffer"/> at which to begin storing the data read from the current stream.</param>
		/// <param name="count">The number of bytes to be read from the current stream.</param>
		/// <exception cref="T:System.ArgumentException">The sum of <paramref name="offset"/> and <paramref name="count"/> is larger than the buffer length.</exception>
		/// <exception cref="T:System.ArgumentNullException"><paramref name="buffer"/> is null.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException"><paramref name="offset"/> or <paramref name="count"/> is negative.</exception>
		public void GetBytes(byte[] buffer, int offset, int count) {
			while(count > 0) {
				var read = randomStream.Read(buffer, offset, count);
				count -= read;
				offset += read;
			}
		}
		
		/// <summary>Get a random <see cref="Boolean"/>.</summary>
		/// <returns>A random boolean.</returns>
		public Boolean GetBoolean() {
			return GetByte() > 127;
		}
		
		/// <summary>Get a random <see cref="Byte"/>.</summary>
		/// <returns>A random byte.</returns>
		public Byte GetByte() {
			lock(randBuffer) {
				GetBytes(randBuffer, 0, sizeof(Byte));
				return randBuffer[0];
			}
		}
		
		/// <summary>Get a random <see cref="Int16"/>.</summary>
		/// <returns>A random short between full short range.</returns>
		public Int16 GetInt16() {
			lock(randBuffer) {
				GetBytes(randBuffer, 0, sizeof(Int16));
				return BitConverter.ToInt16(randBuffer, 0);
			}
		}
		
		/// <summary>Get a random <see cref="Int32"/>.</summary>
		/// <returns>A random integer between full integer range.</returns>
		public Int32 GetInt32() {
			lock(randBuffer) {
				GetBytes(randBuffer, 0, sizeof(Int32));
				return BitConverter.ToInt32(randBuffer, 0);
			}
		}
		
		/// <summary>Get a random <see cref="Int64"/>.</summary>
		/// <returns>A random long between full long range.</returns>
		public Int64 GetInt64() {
			lock(randBuffer) {
				GetBytes(randBuffer, 0, sizeof(Int64));
				return BitConverter.ToInt64(randBuffer, 0);
			}
		}
		
		/// <summary>Get a random <see cref="SByte"/>.</summary>
		/// <returns>A random signed byte between full signed byte range.</returns>
		public SByte GetSByte() {
			return unchecked( (SByte)GetByte() );
		}
		
		/// <summary>Get a random <see cref="UInt16"/>.</summary>
		/// <returns>A random short.</returns>
		public UInt16 GetUInt16() {
			lock(randBuffer) {
				GetBytes(randBuffer, 0, sizeof(UInt16));
				return BitConverter.ToUInt16(randBuffer, 0);
			}
		}
		
		/// <summary>Get a random <see cref="UInt32"/>.</summary>
		/// <returns>A random integer.</returns>
		public UInt32 GetUInt32() {
			lock(randBuffer) {
				GetBytes(randBuffer, 0, sizeof(UInt32));
				return BitConverter.ToUInt32(randBuffer, 0);
			}
		}
		
		/// <summary>Get a random <see cref="UInt64"/>.</summary>
		/// <returns>A random long.</returns>
		public UInt64 GetUInt64() {
			lock(randBuffer) {
				GetBytes(randBuffer, 0, sizeof(UInt64));
				return BitConverter.ToUInt64(randBuffer, 0);
			}
		}
		
		/// <summary>Get a random <see cref="Single"/>.</summary>
		/// <returns>A random normalized single (between 0 inclusive and 1 inclusive).</returns>
		public Single GetSingle() {
			while(true) {
				try {
					return (Single)System.Math.Abs(GetInt32()) / (Single)Int32.MaxValue;
				} catch(OverflowException) { }  // 1 in 2^32 chance to happen.
			}
		}
		
		/// <summary>Get a random <see cref="Double"/>.</summary>
		/// <returns>A random normalized double (between 0 inclusive and 1 inclusive).</returns>
		public Double GetDouble() {
			while(true) {
				try {
					return (Double)System.Math.Abs(GetInt64()) / (Double)Int64.MaxValue;
				} catch(OverflowException) { }  // 1 in 2^64 chance to happen.
			}
		}
		
		/// <summary>Get a random integer between zero and the specified maximum.</summary>
		/// <param name="max">The upper bound exclusive.</param>
		/// <returns>A random integer between specified bounds.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException"><paramref name="max"/> is less than or equal to zero.</exception>
		public int GetInteger(int max) {
			if(max <= 0)
				throw new ArgumentOutOfRangeException("max", "max must be greater than zero.");
			
			return (int)(GetUInt64() % (ulong)max);
		}
		
		/// <summary>Get a random integer between specified bounds.</summary>
		/// <param name="min">The lower bound inclusive.</param>
		/// <param name="max">The upper bound exclusive.</param>
		/// <returns>A random integer between specified bounds.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException"><paramref name="min"/> is greater than <paramref name="max"/>.</exception>
		public int GetInteger(int min, int max) {
			if(min > max)
				throw new ArgumentOutOfRangeException("min", "min cannot be greater than max.");
			
			return (int)((long)(GetUInt64() % (ulong)(max - min)) + min);
		}
		
		/// <summary>Get a random float between zero and the specified maximum.</summary>
		/// <param name="max">The upper bound inclusive.</param>
		/// <returns>A random float between zero and the specified maximum.</returns>
		public float GetFloat(float max) {
			return GetSingle()*max;
		}
		
		/// <summary>Get a random float between the specified bounds.</summary>
		/// <param name="min">The lower bound inclusive.</param>
		/// <param name="max">The upper bound inclusive.</param>
		/// <returns>A random float between the specified bounds.</returns>
		public float GetFloat(float min, float max) {
			return GetFloat(max - min) + min;
		}
		
		/// <summary>Get a random double between zero and the specified maximum.</summary>
		/// <param name="max">The upper bound inclusive.</param>
		/// <returns>A random double between zero and the specified maximum.</returns>
		public double GetDouble(double max) {
			return GetDouble()*max;
		}
		
		/// <summary>Get a random double between the specified bounds.</summary>
		/// <param name="min">The lower bound inclusive.</param>
		/// <param name="max">The upper bound inclusive.</param>
		/// <returns>A random double between the specified bounds.</returns>
		public double GetDouble(double min, double max) {
			return GetDouble(max - min) + min;
		}
	}
}
