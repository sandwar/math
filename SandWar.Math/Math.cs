﻿using System;

namespace SandWar.Math
{
	/// <summary>Provides constants and static methods for common mathematical functions.</summary>
	public static class Math
	{
		/// <summary>A small value used to make comparisons.</summary>
		public const float Epsilon = 1e-6f;
		
		/// <summary>The value of Pi.</summary>
		public const float Pi = (float)System.Math.PI;
		
		/// <summary>Compute the absolute value of the specified <see cref="Single"/>.</summary>
		public static float Abs(float f) {
			return System.Math.Abs(f);
		}
		
		/// <summary>Compute the cosine of a <see cref="Single"/>.</summary>
		public static float Cos(float f) {
			return (float)System.Math.Cos(f);
		}
		
		/// <summary>Compute the cosine of a <see cref="Double"/>.</summary>
		public static double Cos(double d) {
			return System.Math.Cos(d);
		}
		
		/// <summary>Compute the sine of a <see cref="Single"/>.</summary>
		public static float Sin(float f) {
			return (float)System.Math.Sin(f);
		}
		
		/// <summary>Compute the sine of a <see cref="Double"/>.</summary>
		public static double Sin(double d) {
			return System.Math.Sin(d);
		}
		
		/// <summary>Compute the square root of a <see cref="Single"/>.</summary>
		public static float Sqrt(float f) {
			return (float)System.Math.Sqrt(f);
		}
		
		/// <summary>Compute the square root of a <see cref="Double"/>.</summary>
		public static double Sqrt(double d) {
			return System.Math.Sqrt(d);
		}
		
		/// <summary>Compute the tangent of the specified angle.</summary>
		public static float Tan(float f) {
			return (float)System.Math.Tan((double)f);
		}
		
		/// <summary>Compute the tangent of the specified angle.</summary>
		public static double Tan(double d) {
			return System.Math.Tan(d);
		}
		
		/// <summary>Find the smaller of two <see cref="Single"/> numbers.</summary>
		public static float Min(float f1, float f2) {
			return System.Math.Min(f1, f2);
		}
		
		/// <summary>Find the smaller of two <see cref="Double"/> numbers.</summary>
		public static double Min(double d1, double d2) {
			return System.Math.Min(d1, d2);
		}
		
		/// <summary>Find the bigger of two <see cref="Single"/> numbers.</summary>
		public static float Max(float f1, float f2) {
			return System.Math.Max(f1, f2);
		}
		
		/// <summary>Find the bigger of two <see cref="Double"/> numbers.</summary>
		public static double Max(double d1, double d2) {
			return System.Math.Max(d1, d2);
		}
		
		/// <summary>Compute the modulus of an <see cref="Int32"/>.</summary>
		public static int Mod(int i, int m) {
			// TODO: test if it works with edge cases
			
			int r = i % m;
			return r < 0 ? r + m : r;
			
			// TODO: check if the alternate form is faster
			
			// alternate form
			// return (i % m + m) % m;
		}
		
		/// <summary>Compute the modulus of an <see cref="Int64"/>.</summary>
		public static long Mod(long l, long m) {
			// TODO: test if works with numbers out of int range
			
			long r = l % m;
			return r < 0 ? r + m : r;
			
			// TODO: check if the alternate form is faster
			
			// alternate form
			// return (l % m + m) % m;
		}
		
		/// <summary>Get the smallest <see cref="Int32"/> higher than or equal to a <see cref="Single"/>.</summary>
		public static int CeilingInt(float f) {
			return (int)System.Math.Ceiling(f);
		}
	}
}
