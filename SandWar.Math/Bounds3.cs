﻿using System;

namespace SandWar.Math
{
	public struct Bounds3
	{
		public Vector3 Center { get; set; }
		public Vector3 Extents { get; set; }
		
		#region Properties
		
		public Vector3 Min {
			get { return Center - Extents; }
			set { byMinMax(value, Max); }
		}
		public Vector3 Max {
			get { return Center + Extents; }
			set { byMinMax(Min, value); }
		}
		
		public Vector3 Size {
			get { return Extents * 2f; }
			set { Extents = value / 2f; }
		}
		
		#endregion
		
		#region Methods
		
		public bool Contains(Vector3 point) {
			var min = Min;
			var max = Max;
			
			return point.X >= min.X && point.X <= max.X &&
			       point.Y >= min.Y && point.Y <= max.Y &&
			       point.Z >= min.Z && point.Z <= max.Z;
		}
		
		#endregion
		
		#region Overrides
		
		public override bool Equals(object obj) {
			if (obj is Bounds3)
				return Equals((Bounds3)obj);
			
			return false;
		}
		
		public bool Equals(Bounds3 other) {
			return this.Center == other.Center && this.Extents == other.Extents;
		}
		
		public override int GetHashCode() {
			return unchecked((17 * 29 + Center.GetHashCode()) * 29 + Extents.GetHashCode());
		}
		
		public override string ToString() {
			return string.Format("[{0}~{1}]", Min, Max);
		}
		
		#endregion
		
		void byMinMax(Vector3 min, Vector3 max) {
			var actualMin = Vector3.Min(min, max);
			var actualMax = Vector3.Max(min, max);
			
			Size = actualMax - actualMin;
			Center = actualMin + Extents;
		}
	}
}
