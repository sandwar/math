using System;
using System.Collections.Generic;

namespace SandWar.Math
{
	/// <summary>A vector containing 2 floats.</summary>
	public struct Vector2
	{
		#region Constants
		
		/// <summary>Vector at the origin.</summary>
		public static readonly Vector2 Origin = new Vector2( 0f,  0f);
		/// <summary>Vector at Y +1.</summary>
		public static readonly Vector2 Up     = new Vector2( 0f, +1f);
		/// <summary>Vector at Y -1.</summary>
		public static readonly Vector2 Down   = new Vector2( 0f, -1f);
		/// <summary>Vector at X +1.</summary>
		public static readonly Vector2 Left   = new Vector2(+1f,  0f);
		/// <summary>Vector at X -1.</summary>
		public static readonly Vector2 Right  = new Vector2(-1f,  0f);
		
		#endregion
		
		/// <summary>X value.</summary>
		public float X;
		/// <summary>Y value.</summary>
		public float Y;
		
		/// <summary>Construct a vector.</summary>
		/// <param name="x">X value</param>
		/// <param name="y">Y value</param>
		/// <param name="z">Z value</param>
		public Vector2(float x = 0f, float y = 0f) {
			this.X = x;
			this.Y = y;
		}
		
		#region Properties
		
		/// <summary>
		/// Get the square of the magnitude of this vector.
		/// 
		/// This method is faster than <see cref="Magnitude"/>.
		/// </summary>
		public float SquareMagnitude {
			get { return X * X + Y * Y; }
		}
		
		/// <summary>Get the magnitude of this vector.</summary>
		/// <seealso cref="SquareMagnitude"/>
		public float Magnitude {
			get { return Math.Sqrt(SquareMagnitude); }
		}
		
		/// <summary>Get the normalized version of this vector.</summary>
		public Vector2 Normalized {
			get {
				var magnitude = Magnitude;
				return (magnitude < Math.Epsilon) ? (Origin) : (this / magnitude);
			}
		}
		
		/// <summary>Get the absolute value of this vector.</summary>
		public Vector2 Absolute {
			get { return new Vector2(Math.Abs(X), Math.Abs(Y)); }
		}
		
		#endregion
		
		#region Operators
		
		public static Vector2 operator +(Vector2 v1, Vector2 v2) {
			return new Vector2(
				v1.X + v2.X,
				v1.Y + v2.Y
			);
		}
		
		public static Vector2 operator -(Vector2 v1, Vector2 v2) {
			return new Vector2(
				v1.X - v2.X,
				v1.Y - v2.Y
			);
		}
		
		public static Vector2 operator -(Vector2 v) {
			return new Vector2(
				-v.X,
				-v.Y
			);
		}
		
		public static Vector2 operator *(Vector2 v, float f) {
			return new Vector2(
				v.X * f,
				v.Y * f
			);
		}
		
		public static Vector2 operator *(float f, Vector2 v) {
			return new Vector2(
				f * v.X,
				f * v.Y
			);
		}
		
		public static Vector2 operator /(Vector2 v, float f) {
			return new Vector2(
				v.X / f,
				v.Y / f
			);
		}
		
		public static bool operator ==(Vector2 lhs, Vector2 rhs) {
			return (lhs - rhs).SquareMagnitude < Math.Epsilon;
		}
		
		public static bool operator !=(Vector2 lhs, Vector2 rhs) {
			return (lhs - rhs).SquareMagnitude >= Math.Epsilon;
		}
		
		#endregion
		
		#region Methods
		
		/// <summary>Compute the dot product between this vector and another.</summary>
		/// <param name="other">The other vector.</param>
		/// <returns>The dot product.</returns>
		public float Dot(Vector2 other) {
			return X * other.X + Y * other.Y;
		}
		
		#endregion
		
		#region Overrides
		
		public override bool Equals(object obj) {
			if (obj is Vector2)
				return Equals((Vector2)obj);
			
			return false;
		}
		
		public bool Equals(Vector2 other) {
			return Math.Abs(X - other.X) < Math.Epsilon &&
			       Math.Abs(Y - other.Y) < Math.Epsilon;
		}
		
		public override int GetHashCode() {
			// disable NonReadonlyReferencedInGetHashCode
			return unchecked((17 * 29 + X.GetHashCode()) * 29 + Y.GetHashCode());
			// enable NonReadonlyReferencedInGetHashCode
		}
		
		public override string ToString() {
			return string.Format("[{0}, {1}]", X, Y);
		}
		
		#endregion
	}
}
